export function Provider(): void;
export default _default;
// Circular reference from index
export const _default: any;
export function getStore(): any;
export function inject(...args: any[]): any;
export function observer(Component: any): any;
export function setStore(arg: any): void;
